\section{Threat Model} \label{sec:threatmodel}
In this section, we demonstrate how an attacker can theoretically and practically exploit the vulnerability in the heartbeat exchange.
We then describe an automated attack that we have successfully applied to one of the devices.

\subsection{Wi-Fi attacks}
The easiest way for an attacker to exploit heartbeats is by gaining access to the LAN to which the device is connected.
This is relatively straightforward for an attacker to achieve, as many users still use broken protocols like WEP and WPA; many do not even use Wi-Fi encryption at all.
According to Wigle, around 4\% of access points are unencrypted, 6.3\% use WEP and 5.8\% use WPA~\cite{wigle}.
An attacker can also compromise Wi-Fi networks protected by WPA2 using dictionary attacks like the attack demonstrated in~\cite{nakhila2015parallel}.
An attacker can compromise a Wi-Fi AP by exploiting the vulnerability of Wi-Fi Protected Setup (WPS) protocol, which allows association to an AP using an 8-digit PIN.~\cite{viehbock2011brute}. 

\subsection{DNS Server Hijacking}
An attacker can hijack a DNS server and change the records directly.
This may be a primary DNS server of the IoT device's vendor or a resolver or cache server of an ISP.
In all cases the attacker would be able to exploit the vulnerability in a large group of devices that are served by the compromised DNS server.
This attack is infeasible in most situations, but it is still possible and there are several real-world examples, such as the DNSpionage attack, which targeted governmental and private organisations in Lebanon and the UAE.
The attackers redirected email and VPN traffic to an IP address managed by them~\cite{krebs_dns}.
Similarly, attackers can redirect heartbeat traffic to a TCP proxy using this attack and eventually exploit the vulnerability in the heartbeat exchange.

\subsection{Network Attacks}
Theoretically, an attacker can gain access to one or more routers of an ISP by exploiting a vulnerability in the router’s operating system.
Subsequently, the attacker can use the compromised router to spoof DNS responses or to filter out traffic.
This indeed is practically almost infeasible as it is very difficult to gain access to ISP's network devices.
Nonetheless, there are some real-world examples that prove this kind of attacks possible in specific situations.
For instance, in 2015, FireEye discovered that a group of attackers did manage to take over Cisco routers~\cite{ciscorouters}.
Similarly, Cisco has recently discovered and patched serious security holes that allow root access in their SD-WAN software~\cite{sdwan}.
This proves that such attacks although very difficult, are still possible and can allow attackers to eventually exploit the heartbeat vulnerability against not only one but a large group of devices. 

\subsection{Automated Attack against Swann using Evil-Twin Wi-Fi AP}
For devices that support Wi-Fi connections, attackers can try Evil Twin attacks to force a device to connect to their own LAN on which they can spoof DNS records.
We experimented this attack on the Swann hub and successfully managed to force it to connect to our LAN and eventually exploited its heartbeats.
In WPA2, a four-way handshake needs to be completed before sending any data, but the handshake cannot be completed because the evil twin does not know the password.
Surprisingly, Swann hub connected to the rogue AP anyway and sent DHCP, DNS and TCP traffic normally.
After exchanging few packets, the hub then fell back to association request/responses and kept repeating the same behaviour in a loop.
An illustration of this behaviour is shown in Figure~\ref{fig:Swann-automated-attack-setup}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.99\linewidth]{Figures/Swann-automated-attack-setup.jpg}
    \caption{Combination of the WPA2 and Heartbeat vulnerabilities in Swann.}
    \label{fig:Swann-automated-attack-setup}
\end{figure}

The IEEE802.11i standard~\cite{IEEE802.11standard} states that an AP and a Station (STA) must exchange four EAPoL messages to complete the 4-way handshake for authentication before sending any data.
Based on the IEEE802.11i standard we deduced that this is a misimplementation of the standard in the driver of the devices' wireless chipset.
By analysing Swann hub’s hardware, we discovered that it uses a Jorjin WG7831-D0 Wi-Fi chipset.
According to its documentation, the chipset is based on the Texas Instruments WL1831MOD chipset and WiLink8 driver~\cite{jorjin_module,jorjin_support_note}.

By exploiting this misimplementation, we managed to achieve a MITM between the hub and the server, and perform the heartbeat attack.
The attack was then automated using bash and the Python proxy script.
The whole process can be found on GitHub\footnote{\url{https://github.com/SRJanel/SWO\_exploit}}.
Consequently, our exploit can disable any Swann device located nearby our attacking machine, without its owner's awareness.