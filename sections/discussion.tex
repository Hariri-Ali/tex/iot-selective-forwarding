\section{Discussion} \label{sec:discussion}
Exploiting the heartbeat pattern is not critical in all IoT applications like light bulbs for example.
However, it can be remarkably critical in home security kits and in some health care applications which are used to remotely monitor patients.
Furthermore, this attack can be extended to identify other patterns in encrypted traffic like sensor readings or controller commands and selectively forward and drop those patterns.
For instance, attackers can block some control commands in water distribution systems to cause floods, or in electricity distribution systems to cause blackouts.
Autonomous vehicles can also be affected by this attack in which attackers filter communications between the vehicles leading to deadly accidents.
Therefore, detection and defence mechanisms must be studied and developed to prevent such attacks and to reach a more secure IoT.
To exploit heartbeats, an attacker must redirect network traffic to a malicious proxy.
Nevertheless, attackers can find many ways to gain access over the network.
This was proven by Chapman ~\cite{lifx-bulbs} who compromised WLAN credentials by exploiting a vulnerability in LIFX light bulbs.
Alternatively, attackers can compromise Wi-Fi credentials using social engineering techniques.
Therefore, it is necessary to secure the heartbeat exchange in IoT, even though its exploit requires network access.

\subsection{Recommendations and Countermeasures}
Some "secure" implementations of the heartbeat pattern were proposed by different authors.
For instance, IBM proposes an implementation that uses freshness and challenge-response mechanisms and provides security against spoofing and replay attacks~\cite{IBMarticle}.
However, this solution is not secure against our attack, because the proxy can simply detect the challenge-response pattern due to its periodic nature and selectively forward it, while blocking any other traffic.
For that, we propose some recommendations and countermeasures that can make heartbeat exchanges more secure in IoT.
Firstly, heartbeats must always be implemented on the application layer instead of the transport layer like TCP keepalive packets.
Secondly, heartbeats must include information about the last message that was sent before the heartbeat.
This would allow both endpoints to detect if any messages were filtered out.
To understand this mechanism better, consider this scenario:

\begin{itemize}
    \item Device sends an encrypted heartbeat. It contains a sequence number of the last message sent, which was a heartbeat too.
    \item Server confirms that the previous message was indeed a heartbeat.
    \item Device sends an alert that also contains previous message's sequence number.
    \item Attacker selectively blocks the alert. 
    \item Device sends a heartbeat message that contains a sequence number of the alert that was blocked.
    \item Server understands that the alert was not delivered due to the missing sequence number. 
    \item Finally, server notifies the user that alerts are being filtered out.
\end{itemize}

A prevention mechanism would be to always pass the last message within the heartbeat.
This would allow the server to receive the blocked message and detect the attack because it did not receive the message as a stand-alone.
Another prevention would be using IPSec as it prevents redirecting traffic to a proxy.

The proposed countermeasure will affect the efficiency of the IoT system especially if the devices have limited resources.
This is due to the additional metadata the must be sent in heartbeats or the overhead introduced by IPSec.
However, security usually comes at the cost of performance.

\subsection{Responsible Disclosure Exercise}
We contacted most of the manufacturers of the affected devices.
Furthermore, we conducted more in-depth conversation with Swann since its device has another serious flaw.
We also contacted Texas Instruments, which manufactured the Wi-Fi chipset used by the Swann device under our investigation.

D-Link confirmed our findings but decided that the occurrence of such attack is uncommon and eventually lowered the development priority of a patch.
Swann also confirmed our findings and reported that they will release a security patch for their products.
Panasonic stated that the discovered pattern is not a heartbeat, but rather a mechanism to establish a peer-to-peer connection between the device and the mobile application.
Thus, they think that there is no need to release a patch.
Texas Instruments tried to reproduce our WPA2 results in Swann.
They tested several versions of their driver and all proved to be secure.
They also sent us an evaluation module to test it ourselves and we do confirm their results.
After thorough investigations by Swann, they confirmed that the module was installed by a company that went out of operation, so although the root cause has not been found, the vulnerability most likely stems from the specific WPA supplicant and software used in the product.
This means that the company that installed the module is probably responsible for the poor implementation