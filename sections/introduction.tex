\section{Introduction}\label{sec:introduction}
The Internet has considerably changed in the last decade.
It has become more than just a platform for email exchanges, web browsing, instant messaging or media streaming.
The connected devices are no longer just servers, computers and smartphones, but instead the Internet has become an Internet of Things (IoT), of connected wearables, home appliances, biomedical devices, cars, cities, and many more.
IoT is gaining more and more popularity and experts predict that it will become an Internet of Everything in the near future~\cite{IoE}.
It is estimated that the number of connected devices will reach up to 20 billion by 2020~\cite{gartner}.

Despite its convenience, IoT and its applications introduce major privacy threats and critical security risks. For instance, IoT devices may be compromised to access personal information, or to gain control over industries, cities and public organisations or disrupt their services.
This has been shown by several incidents like the Mirai botnet~\cite{antonakakis2017understanding, cetin2019cleaning}.
Mirai is a computer worm that compromised hundreds of thousands of IoT devices, which were then used to mount a Distributed Denial of Service (DDoS) attack to disrupt well-known services like Netflix and Twitter~\cite{mirai}.
IoT also imposes personal privacy threats through smart home devices like cameras, personal assistants and home automation kits.
For example, Trendnet home security and monitoring cameras were found to be vulnerable, allowing an attacker to access live video feeds of the camera without any authentication~\cite{trendnet}.
Likewise, customers of Swann home cameras reported that they were able to access recordings from cameras of other customers~\cite{swann}.
A very recent report of security vulnerabilities in three specialist car alarm systems further illustrates the danger of connecting your device to the Internet without proper security testing~\cite{CarAlarm}.
These vulnerabilities allowed attackers to steal or hijack affected vehicles through the compromised alarm system.
What is ironic here is that whoever bought these vulnerable car alarm systems did so out of a desire to improve the security of their vehicle.
But inadvertently, they introduced security vulnerabilities that would allow attackers to take control of their vehicle. This irony resonates with the message we aim to convey in our paper.

Evidently, the security of IoT devices is a major issue that needs to be continuously evaluated and addressed due to its impact on the physical world.
This motivated us to explore new and common vulnerabilities in a selected set of consumer IoT products.
Particularly, we targeted home security devices because their security is critical as people may rely on them to protect their homes.

{\bf {Contribution.}} 
The main contribution of this paper is the {\em {exploitation of a vulnerability in the heartbeat exchange of IoT devices}}.
By exploiting this vulnerability, an attacker can disable IoT home security devices without the awareness of their owners.
Particularly, the device will appear to be online and working normally, but in fact it will be completely disabled.
This was due to wrong implementations of heartbeat messages exchanged between devices and their cloud infrastructure.
Our second contribution relates to a {\em {potential misimplementation of the WPA2 four-way handshake protocol in some IoT devices}}.
This misimplementation allows an attacker to carry out an evil twin access point attack, which would force the device to connect to the attacker's LAN.
This would allow the attacker to exploit further vulnerabilities, or eavesdrop on the communications between the device and the cloud.

The rest of this paper is organised as follows.
Section~\ref{sec:relatedwork} presents related work.
Section~\ref{sec:methodology} outlines our methodology, while Section~\ref{sec:results} presents our findings and results. 
Section~\ref{sec:threatmodel} describes the threat model to illustrate the feasibility of our attack.
Section~\ref{sec:discussion} dissects the risks and consequences of the discovered vulnerability.
We present some insights from the responsible disclosure exercise we carried out with the affected manufacturers of the devices.
We also propose some recommendations to how to fix the vulnerabilities uncovered by our research.
Finally, Section~\ref{sec:conclusion} concludes the paper and provides ideas for future work.